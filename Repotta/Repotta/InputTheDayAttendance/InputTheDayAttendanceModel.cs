﻿using System;
using System.Linq;
using System.Windows.Forms;
using Repotta.DBClass;
using static Repotta.Common.Define;
using static Repotta.Common.Attributes;

namespace Repotta.InputTheDayAttendance
{
    public class InputTheDayAttendanceModel
    {
        #region メンバ変数
        private WorkingMasterRow FWorkingMasterRow = null;
        private DefaultInfoMasterRow FDefaultInfoMasterRow = null;
        private EmployeeMasterRow FEmployeeMasterRow = null;
        private FormInputTheDayAttendance FForm = null;

        #region DB Get Set
        /// <summary>
        /// 年月日
        /// </summary>
        public string DB_WorkingDate
        {
            set => FWorkingMasterRow.WorkingDate = value;
        }

        /// <summary>
        /// 休日フラグ
        /// </summary>
        public bool DB_Holiday
        {
            get => FWorkingMasterRow != null
                    ? FWorkingMasterRow.HolidayFlag == 1
                    : FDefaultInfoMasterRow.HolidayFlag == 1;
            set => FWorkingMasterRow.HolidayFlag = value ? 1 : 0;
        }

        /// <summary>
        /// 開始時間（時）
        /// </summary>
        public string DB_HourStart
        {
            get => string.IsNullOrEmpty(FWorkingMasterRow?.WorkingStart)
                    ? FDefaultInfoMasterRow.WorkingStart.Substring(0, 2)
                    : FWorkingMasterRow.WorkingStart.Substring(0, 2);
            set => FWorkingMasterRow.WorkingStart =
                $"{value}{FWorkingMasterRow.WorkingStart.Substring(2, 2)}";
        }

        /// <summary>
        /// 開始時間（分）
        /// </summary>
        public string DB_MinStart
        {
            get => string.IsNullOrEmpty(FWorkingMasterRow?.WorkingStart)
                    ? FDefaultInfoMasterRow.WorkingStart.Substring(2, 2)
                    : FWorkingMasterRow.WorkingStart.Substring(2, 2);
            set => FWorkingMasterRow.WorkingStart =
                $"{FWorkingMasterRow.WorkingStart.Substring(0, 2)}{value}";
        }

        /// <summary>
        /// 終了時間（時）
        /// </summary>
        public string DB_HourEnd
        {
            get => string.IsNullOrEmpty(FWorkingMasterRow?.WorkingEnd)
                    ? FDefaultInfoMasterRow.WorkingEnd.Substring(0, 2)
                    : FWorkingMasterRow.WorkingEnd.Substring(0, 2);
            set => FWorkingMasterRow.WorkingEnd =
                $"{value}{FWorkingMasterRow.WorkingEnd.Substring(2, 2)}";
        }

        /// <summary>
        /// 終了時間（分）
        /// </summary>
        public string DB_MinEnd
        {
            get => string.IsNullOrEmpty(FWorkingMasterRow?.WorkingEnd)
                    ? FDefaultInfoMasterRow.WorkingEnd.Substring(2, 2)
                    : FWorkingMasterRow.WorkingEnd.Substring(2, 2);
            set => FWorkingMasterRow.WorkingEnd =
                $"{FWorkingMasterRow.WorkingEnd.Substring(0, 2)}{value}";
        }

        /// <summary>
        /// 休憩時間
        /// </summary>
        public Decimal DB_BreakTime
        {
            get => FWorkingMasterRow != null ? FWorkingMasterRow.BreakTime : FDefaultInfoMasterRow.BreakTime;
            set => FWorkingMasterRow.BreakTime = value;
        }

        /// <summary>
        /// 自社業務時間
        /// </summary>
        public Decimal DB_TheirWorkingTime
        {
            get => FWorkingMasterRow != null ? FWorkingMasterRow.TheirWorkingTime : FDefaultInfoMasterRow.TheirWorking;
            set => FWorkingMasterRow.TheirWorkingTime = value;
        }

        /// <summary>
        /// 通常勤務時間
        /// </summary>
        public Decimal DB_NormalWorkingTime
        {
            get => FWorkingMasterRow != null ? FWorkingMasterRow.NomalWorking : FDefaultInfoMasterRow.NomalWorking;
            set => FDefaultInfoMasterRow.NomalWorking = value;
        }

        /// <summary>
        /// 休暇フラグ
        /// </summary>
        public bool DB_Vacation
        {
            get => (FWorkingMasterRow?.VacationFlag ?? (int?)0) == 1;
            set => FWorkingMasterRow.VacationFlag = value ? 1 : 0;
        }

        /// <summary>
        /// 休暇種別
        /// </summary>
        public VacationTypeEnum DB_VacationType
        {
            get => (VacationTypeEnum)(FWorkingMasterRow?.VacationType ?? (int?)0);
            set => FWorkingMasterRow.VacationType = (int)value;
        }

        /// <summary>
        /// 欠勤フラグ
        /// </summary>
        public bool DB_Absence
        {
            get => (FWorkingMasterRow?.AbsenceFlag ?? (int?)0) == 1;
            set => FWorkingMasterRow.AbsenceFlag = value ? 1 : 0;
        }

        /// <summary>
        /// 欠勤届有無
        /// </summary>
        public NotificationEnum DB_AbsenceSession
        {
            get => (NotificationEnum)(FWorkingMasterRow?.AbsenceSessionFlag ?? (int?)0);
            set => FWorkingMasterRow.AbsenceSessionFlag = (int)value;
        }

        /// <summary>
        /// 遅刻・早退・私用外出フラグ
        /// </summary>
        public bool DB_LateEarlyOut
        {
            get => (FWorkingMasterRow?.LateEarlyOutFlag ?? (int?)0) == 1;
            set => FWorkingMasterRow.LateEarlyOutFlag = value ? 1 : 0;
        }

        /// <summary>
        /// 遅刻・早退・私用外出種別
        /// </summary>
        public LateEarlyOutTypeEnum DB_LateEarlyOutType
        {
            get => (LateEarlyOutTypeEnum)(FWorkingMasterRow?.LateEarlyOutType ?? (int?)0);
            set => FWorkingMasterRow.LateEarlyOutType = (int)value;
        }

        /// <summary>
        /// 遅刻・早退・私用外出届フラグ
        /// </summary>
        public NotificationEnum DB_LateEarlyOutSession
        {
            get => (NotificationEnum)(FWorkingMasterRow?.LateEarlyOutSessionType ?? (int?)0);
            set => FWorkingMasterRow.LateEarlyOutSessionType = (int)value;
        }

        /// <summary>
        /// 遅刻・早退・私用外出時間
        /// </summary>
        public string DB_LateEarlyOutTime
        {
            get => FWorkingMasterRow?.LateEarlyOutTime;
            set => FWorkingMasterRow.LateEarlyOutTime = value;
        }

        /// <summary>
        /// 備考
        /// </summary>
        public string DB_Remarks
        {
            get => FWorkingMasterRow?.Remarks;
            set => FWorkingMasterRow.Remarks = value;
        }
        #endregion DB Get Set

        #region Form Get Set
        /// <summary>
        /// 年月日
        /// </summary>
        public string Form_WorkingDate => FForm.dtpDate.Value.ToString(C_DateTimeFormat);

        /// <summary>
        /// 休日フラグ
        /// </summary>
        public bool Form_Holiday
        {
            get => !FForm.cbHoliday.Enabled ? false :
                    FForm.cbHoliday.Checked;
            set => FForm.cbHoliday.Checked = value;
        }

        /// <summary>
        /// 開始時間（時）
        /// </summary>
        public string Form_HourStart
        {
            get => !FForm.cmbHourStart.Enabled ? C_TimeDefault : FForm.cmbHourStart.Text;
            set => SetComboboxValue(FForm.cmbHourStart, value);
        }

        /// <summary>
        /// 開始時間（分）
        /// </summary>
        public string Form_MinStart
        {
            get => !FForm.cmbMinStart.Enabled ? C_TimeDefault : FForm.cmbMinStart.Text;
            set => SetComboboxValue(FForm.cmbMinStart, value);
        }

        /// <summary>
        /// 終了時間（時）
        /// </summary>
        public string Form_HourEnd
        {
            get => !FForm.cmbHourEnd.Enabled ? C_TimeDefault : FForm.cmbHourEnd.Text;
            set => SetComboboxValue(FForm.cmbHourEnd, value);
        }

        /// <summary>
        /// 終了時間（分）
        /// </summary>
        public string Form_MinEnd
        {
            get => !FForm.cmbMinEnd.Enabled ? C_TimeDefault : FForm.cmbMinEnd.Text;
            set => SetComboboxValue(FForm.cmbMinEnd, value);
        }

        /// <summary>
        /// 休憩時間
        /// </summary>
        public Decimal Form_BreakTime
        {
            get => !FForm.tbBreakTime.Enabled ? 0m :
                    decimal.TryParse(FForm.tbBreakTime.Text, out decimal wDecVal) ? wDecVal : 0m;
            set => FForm.tbBreakTime.Text = value.ToString();
        }

        /// <summary>
        /// 自社業務時間
        /// </summary>
        public Decimal Form_TheirWorkingTime
        {
            get => !FForm.tbTheirWorkingTime.Enabled ? 0m :
                    decimal.TryParse(FForm.tbTheirWorkingTime.Text, out decimal wDecVal) ? wDecVal : 0m;
            set => FForm.tbTheirWorkingTime.Text = value.ToString();
        }

        /// <summary>
        /// 通常勤務時間
        /// </summary>
        public Decimal Form_NormalWorkingTime
        {
            get => !FForm.tbNormalWorkingTime.Enabled ? 0m :
                    decimal.TryParse(FForm.tbNormalWorkingTime.Text, out decimal wDecVal) ? wDecVal : 0m;
            set => FForm.tbNormalWorkingTime.Text = value.ToString();
        }

        /// <summary>
        /// 休暇フラグ
        /// </summary>
        public bool Form_Vacation
        {
            get => !FForm.cbVacation.Enabled ? false :
                    FForm.cbVacation.Checked;
            set => FForm.cbVacation.Checked = value;
        }

        /// <summary>
        /// 休暇種別
        /// </summary>
        public VacationTypeEnum Form_VacationType
        {
            get => !FForm.cmbVacationType.Enabled ? VacationTypeEnum.Unknown :
                    (VacationTypeEnum)GetEnumFromText(FForm.cmbVacationType.Text, typeof(VacationTypeEnum));
            set => SetComboboxValue(FForm.cmbVacationType, value);
        }

        /// <summary>
        /// 欠勤フラグ
        /// </summary>
        public bool Form_Absence
        {
            get => !FForm.cbAbsence.Enabled ? false :
                    FForm.cbAbsence.Checked;
            set => FForm.cbAbsence.Checked = value;
        }

        /// <summary>
        /// 欠勤届有無
        /// </summary>
        public NotificationEnum Form_AbsenceSession
        {
            get => !FForm.cmbAbsenceSession.Enabled ? NotificationEnum.Unknown :
                    (NotificationEnum)GetEnumFromText(FForm.cmbAbsenceSession.Text, typeof(NotificationEnum));
            set => SetComboboxValue(FForm.cmbAbsenceSession, value);
        }

        /// <summary>
        /// 遅刻・早退・私用外出フラグ
        /// </summary>
        public bool Form_LateEarlyOut
        {
            get => !FForm.cbLateEarlyOut.Enabled ? false :
                    FForm.cbLateEarlyOut.Checked;
            set => FForm.cbLateEarlyOut.Checked = value;
        }

        /// <summary>
        /// 遅刻・早退・私用外出種別
        /// </summary>
        public LateEarlyOutTypeEnum Form_LateEarlyOutType
        {
            get => !FForm.cmbLateEarlyOutType.Enabled ? LateEarlyOutTypeEnum.Unknown :
                    (LateEarlyOutTypeEnum)GetEnumFromText(FForm.cmbLateEarlyOutType.Text, typeof(LateEarlyOutTypeEnum));
            set => SetComboboxValue(FForm.cmbLateEarlyOutType, value);
        }

        /// <summary>
        /// 遅刻・早退・私用外出届フラグ
        /// </summary>
        public NotificationEnum Form_LateEarlyOutSession
        {
            get => !FForm.cmbLateEarlyOutSession.Enabled ? NotificationEnum.Unknown :
                    (NotificationEnum)GetEnumFromText(FForm.cmbLateEarlyOutSession.Text, typeof(NotificationEnum));
            set => SetComboboxValue(FForm.cmbLateEarlyOutSession, value);
        }
        /// <summary>
        /// 遅刻・早退・私用外出時間
        /// </summary>
        public string Form_LateEarlyOutTime
        {
            get => !FForm.tbLateEarlyOutTime.Enabled ? "0" :
                    FForm.tbLateEarlyOutTime.Text;
            set => FForm.tbLateEarlyOutTime.Text = value;
        }

        /// <summary>
        /// 備考
        /// </summary>
        public string Form_Remarks
        {
            get => FForm.tbRemarks.Text;
            set => FForm.tbRemarks.Text = value;
        }
        #endregion Form Get Set
        #endregion メンバ変数

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public InputTheDayAttendanceModel(FormInputTheDayAttendance vForm)
        {
            FForm = vForm;
            // TODO: ini情報取得

            // TODO: ini情報、現在日付でDB情報取得
            FWorkingMasterRow = new WorkingMaster("00074", Form_WorkingDate).RecordList.FirstOrDefault();
            FDefaultInfoMasterRow = new DefaultInfoMaster("00074", Form_WorkingDate).RecordList.FirstOrDefault();
            FEmployeeMasterRow = new EmployeeMaster("00074").RecordList.FirstOrDefault();

            // 勤務マスタ、基本勤怠情報マスタのどちらもレコードが取得出来ない場合、エラーとする
            if (FWorkingMasterRow == null && FDefaultInfoMasterRow == null)
                new Exception("日付、ユーザIDに紐づく情報が取得できませんでした。");

            SetToForm();
        }
        #endregion コンストラクタ

        #region Method
        /// <summary>
        /// 登録
        /// </summary>
        public void Execute()
        {
            SetToDB();
            FWorkingMasterRow.Execute(() => FWorkingMasterRow.GetQueryUpdate());
        }

        /// <summary>
        /// チェック処理
        /// </summary>
        /// <param name="vMessage"></param>
        /// <returns></returns>
        public bool Check(out string vMessage)
        {
            #region 勤務時間超過確認
            {
                string wOver = "勤務時間を超過しています。";
                decimal wTime = decimal.Parse(Form_HourEnd) - decimal.Parse(Form_HourStart);
                if (wTime < 0) wTime += 24;
                wTime += (decimal.Parse(Form_MinEnd) - decimal.Parse(Form_MinStart)) / 60;
                if (Form_BreakTime > wTime)
                {
                    vMessage = $"休憩(外出)が{wOver}";
                    return false;
                }
                if (Form_TheirWorkingTime > wTime)
                {
                    vMessage = $"自社業務が{wOver}";
                    return false;
                }
                if(Form_BreakTime + Form_TheirWorkingTime > wTime)
                {
                    vMessage = $"休憩(外出)、自社業務の合計が{wOver}";
                    return false;
                }
                #region 私用外出時
                if (Form_LateEarlyOutType == LateEarlyOutTypeEnum.Out)
                {
                    var wLateEarlyOutTime = decimal.Parse(Form_LateEarlyOutTime);
                    if (wLateEarlyOutTime > wTime)
                    {
                        vMessage = $"私用外出時間が{wOver}";
                        return false;
                    }
                    if(wLateEarlyOutTime + Form_BreakTime > wTime)
                    {
                        vMessage = $"休憩(外出)、私用外出時間の合計が{wOver}";
                        return false;
                    }
                    if(wLateEarlyOutTime + Form_TheirWorkingTime > wTime)
                    {
                        vMessage = $"自社業務、私用外出時間の合計が{wOver}";
                        return false;
                    }
                    if (wLateEarlyOutTime + Form_TheirWorkingTime + Form_BreakTime > wTime)
                    {
                        vMessage = $"休憩(外出)、自社業務、私用外出時間の合計が{wOver}";
                        return false;
                    }
                }
                #endregion 私用外出時
            }
            #endregion 勤務時間超過確認
            vMessage = string.Empty;
            return true;
        }

        /// <summary>
        /// DB更新用値セット
        /// </summary>
        private void SetToDB()
        {
            if (FWorkingMasterRow == null) FWorkingMasterRow = new WorkingMasterRow(FEmployeeMasterRow);
            DB_WorkingDate = Form_WorkingDate;
            DB_Holiday = Form_Holiday;
            DB_HourStart = Form_HourStart;
            DB_MinStart = Form_MinStart;
            DB_HourEnd = Form_HourEnd;
            DB_MinEnd = Form_MinEnd;
            DB_BreakTime = Form_BreakTime;
            DB_TheirWorkingTime = Form_TheirWorkingTime;
            DB_NormalWorkingTime = Form_NormalWorkingTime;
            DB_Vacation = Form_Vacation;
            DB_VacationType = Form_VacationType;
            DB_Absence = Form_Absence;
            DB_AbsenceSession = Form_AbsenceSession;
            DB_LateEarlyOut = Form_LateEarlyOut;
            DB_LateEarlyOutType = Form_LateEarlyOutType;
            DB_LateEarlyOutSession = Form_LateEarlyOutSession;
            DB_LateEarlyOutTime = Form_LateEarlyOutTime;
            DB_Remarks = Form_Remarks;
            FWorkingMasterRow.UpdateDate = DateTime.Now.ToString(C_UpdateTimeFormat);
        }

        /// <summary>
        /// DBレコード値を画面に表示
        /// </summary>
        private void SetToForm()
        {
            Form_Holiday = DB_Holiday;
            Form_HourStart = DB_HourStart;
            Form_MinStart = DB_MinStart;
            Form_HourEnd = DB_HourEnd;
            Form_MinEnd = DB_MinEnd;
            Form_BreakTime = DB_BreakTime;
            Form_TheirWorkingTime = DB_TheirWorkingTime;
            Form_NormalWorkingTime = DB_NormalWorkingTime;
            Form_Vacation = DB_Vacation;
            Form_VacationType = DB_VacationType;
            Form_Absence = DB_Absence;
            Form_AbsenceSession = DB_AbsenceSession;
            Form_LateEarlyOut = DB_LateEarlyOut;
            Form_LateEarlyOutType = DB_LateEarlyOutType;
            Form_LateEarlyOutSession = DB_LateEarlyOutSession;
            Form_LateEarlyOutTime = DB_LateEarlyOutTime;
            Form_Remarks = DB_Remarks;
        }

        /// <summary>
        /// コンボボックスの初期値設定
        /// </summary>
        /// <param name="vCombobox"></param>
        /// <param name="vValue"></param>
        private void SetComboboxValue(ComboBox vCombobox, Enum vValue) =>
            SetComboboxValue(vCombobox, vValue.GetText());

        /// <summary>
        /// コンボボックスの初期値設定
        /// </summary>
        /// <param name="vCombobox"></param>
        /// <param name="vValue"></param>
        private void SetComboboxValue(ComboBox vCombobox, string vValue)
        {
            var wIndex = vCombobox.Items.IndexOf(vValue);
            vCombobox.SelectedIndex = wIndex == -1 ? 0 : wIndex;
        }
        #endregion Method
    }
}
