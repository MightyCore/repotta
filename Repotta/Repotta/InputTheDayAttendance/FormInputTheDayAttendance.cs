﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.Collections.Generic;
using Repotta.InputTheDayAttendance;
using static Repotta.Common.Define;
using static Repotta.Common.Attributes;

namespace Repotta
{
    public partial class FormInputTheDayAttendance : FormBase
    {
        #region メンバ変数
        private InputTheDayAttendanceModel FModel = null;
        #endregion メンバ変数

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public FormInputTheDayAttendance()
        {
            InitializeComponent();
            SetSetting();
        }
        #endregion コンストラクタ

        #region プロパティ
        /// <summary>
        /// フォーム画面設定
        /// </summary>
        private void SetSetting()
        {
            SetComboBox(this.cmbVacationType, x => SetComboBox(x, typeof(VacationTypeEnum), new Enum[] { VacationTypeEnum.Unknown }));
            SetComboBox(this.cmbLateEarlyOutType, x => SetComboBox(x, typeof(LateEarlyOutTypeEnum), new Enum[] { LateEarlyOutTypeEnum.Unknown }));
            SetComboBox(this.cmbAbsenceSession, x => SetComboBox(x, typeof(NotificationEnum), new Enum[] { NotificationEnum.Unknown }));
            SetComboBox(this.cmbLateEarlyOutSession, x => SetComboBox(x, typeof(NotificationEnum), new Enum[] { NotificationEnum.Unknown }));
            SetComboBox(this.cmbHourStart, x => SetComboBox(x, (int)TimeEnum.MaxHour));
            SetComboBox(this.cmbMinStart, x => SetComboBox(x, (int)TimeEnum.MaxMin));
            SetComboBox(this.cmbHourEnd, x => SetComboBox(x, (int)TimeEnum.MaxHour));
            SetComboBox(this.cmbMinEnd, x => SetComboBox(x, (int)TimeEnum.MaxMin));
            dtpDate.Value = DateTime.Now;
            cbVacation_CheckedChanged(this.cbVacation, null);
        }
        #endregion プロパティ

        #region イベント
        /// <summary>
        /// メニューボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMenu_Click(object sender, EventArgs e)
        {
            // TODO: Menu画面へ遷移処理に変更
            MessageBox.Show("メニューがありません。");
        }

        /// <summary>
        /// 登録ボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRegist_Click(object sender, EventArgs e)
        {
            if (!FModel.Check(out string wMessage))
            {
                MessageBox.Show(wMessage);
                return;
            }

            if (MessageBox.Show(this, "登録しても宜しいですか？", "登録", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;

            try
            {
                FModel.Execute();
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }

            MessageBox.Show("登録が完了しました。");
        }

        /// <summary>
        /// 休暇チェックボックス操作時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbVacation_CheckedChanged(object sender, EventArgs e)
        {
            this.cmbVacationType.Enabled = ((CheckBox)sender).Checked;
            this.cbAbsence.Enabled =
            this.cbLateEarlyOut.Enabled = !((CheckBox)sender).Checked;
            SetHolidayItems(!((CheckBox)sender).Checked);
            if (!((CheckBox)sender).Checked) return;
            cmbVacationType_SelectedValueChanged(this.cmbVacationType, null);
        }

        /// <summary>
        /// 休暇種別コンボボックス操作時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbVacationType_SelectedValueChanged(object sender, EventArgs e)
        {
            var wEnabled = ((ComboBox)sender).SelectedItem?.ToString() == VacationTypeEnum.HalfDayOff.GetText();
            this.cbLateEarlyOut.Enabled = wEnabled;
            SetHolidayItems(wEnabled);
        }

        /// <summary>
        /// 欠勤チェックボックス操作時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbAbsence_CheckedChanged(object sender, EventArgs e)
        {
            this.cmbAbsenceSession.Enabled = ((CheckBox)sender).Checked;
            this.cbVacation.Enabled =
            this.cbLateEarlyOut.Enabled = !((CheckBox)sender).Checked;
            SetHolidayItems(!((CheckBox)sender).Checked);
        }

        /// <summary>
        /// 遅刻・早退・私用外出チェックボックス操作時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbLateEarlyOut_CheckedChanged(object sender, EventArgs e)
        {
            this.cbAbsence.Enabled = !((CheckBox)sender).Checked;
            SetLateEarlyOutItems(((CheckBox)sender).Checked);
        }

        /// <summary>
        /// 休日リストの操作可否
        /// </summary>
        /// <param name="vEnable"></param>
        private void SetHolidayItems(bool vEnabled)
        {
            this.cmbHourStart.Enabled =
            this.cmbMinStart.Enabled =
            this.cmbHourEnd.Enabled =
            this.cmbMinEnd.Enabled =
            this.tbBreakTime.Enabled =
            this.tbTheirWorkingTime.Enabled = vEnabled;
            SetLateEarlyOutItems(vEnabled ? this.cbLateEarlyOut.Checked : vEnabled);
        }

        /// <summary>
        /// 遅刻・早退・私用外出リストの操作可否
        /// </summary>
        /// <param name="vEnabled"></param>
        private void SetLateEarlyOutItems(bool vEnabled)
        {
            this.cmbLateEarlyOutType.Enabled =
            this.cmbLateEarlyOutSession.Enabled =
            this.tbLateEarlyOutTime.Enabled = vEnabled;
        }

        /// <summary>
        /// 日付変更
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                FModel = new InputTheDayAttendanceModel(this);
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }
        #endregion イベント
    }
}
