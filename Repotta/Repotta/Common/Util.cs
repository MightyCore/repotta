﻿using System;
using System.Data;
using System.Linq;
using Repotta.DBClass;
using static Repotta.Common.Define;

namespace Repotta.Common
{
    public static class Util
    {
        public static DataTable GetDummyTable(DBBase.DBTablesEnum vTable)
        {
            DataTable wDataTable = new DataTable();
            Action<Enum, object> wSetValue = (x, y) => wDataTable.Rows[0][x.ToString()] = y?.ToString();
            switch (vTable)
            {
                case DBBase.DBTablesEnum.WorkingMaster:
                    Enum.GetValues(typeof(DBBase.WorkingMasterEnum)).Cast<Enum>().ToList().ForEach(x =>
                    {
                        wDataTable.Columns.Add(x.ToString());
                    });
                    #region SetValue
                    wDataTable.Rows.Add();
                    wSetValue(DBBase.WorkingMasterEnum.working_date, DateTime.Now.ToString(C_DateTimeFormat));
                    wSetValue(DBBase.WorkingMasterEnum.employee_number, "00074");
                    wSetValue(DBBase.WorkingMasterEnum.post_name, "GL");
                    wSetValue(DBBase.WorkingMasterEnum.company_name, "SIC");
                    wSetValue(DBBase.WorkingMasterEnum.department_name, "ビジネスソリューション部");
                    wSetValue(DBBase.WorkingMasterEnum.holiday_flag, "0");
                    wSetValue(DBBase.WorkingMasterEnum.working_start, "0700");
                    wSetValue(DBBase.WorkingMasterEnum.working_end, "2000");
                    wSetValue(DBBase.WorkingMasterEnum.break_time, 1.00m);
                    wSetValue(DBBase.WorkingMasterEnum.their_working_time, 8.00m);
                    wSetValue(DBBase.WorkingMasterEnum.nomal_working, 8.00m);
                    wSetValue(DBBase.WorkingMasterEnum.their_working_number, 0.00m);
                    wSetValue(DBBase.WorkingMasterEnum.guests_nomal_working, 8.00m);
                    wSetValue(DBBase.WorkingMasterEnum.guests_over_working, 0.00m);
                    wSetValue(DBBase.WorkingMasterEnum.guests_working, 8.00m);
                    wSetValue(DBBase.WorkingMasterEnum.weekdays_over, 0.00m);
                    wSetValue(DBBase.WorkingMasterEnum.weekdays_night, 0.00m);
                    wSetValue(DBBase.WorkingMasterEnum.holiday_over, 0.00m);
                    wSetValue(DBBase.WorkingMasterEnum.holiday_night, 0.00m);
                    wSetValue(DBBase.WorkingMasterEnum.vacation_flag, "0");
                    wSetValue(DBBase.WorkingMasterEnum.vacation_type, "0");
                    wSetValue(DBBase.WorkingMasterEnum.absence_flag, "0");
                    wSetValue(DBBase.WorkingMasterEnum.absence_session_flag, "0");
                    wSetValue(DBBase.WorkingMasterEnum.late_early_out_flag, "0");
                    wSetValue(DBBase.WorkingMasterEnum.late_early_out_type, "1");
                    wSetValue(DBBase.WorkingMasterEnum.late_early_out_session_type, "0");
                    wSetValue(DBBase.WorkingMasterEnum.late_early_out_time, "0");
                    wSetValue(DBBase.WorkingMasterEnum.remarks, "Test");
                    wSetValue(DBBase.WorkingMasterEnum.update_date, "201812241741");
                    #endregion SetValue
                    break;
                case DBBase.DBTablesEnum.DefaultInfoMaster:
                    Enum.GetValues(typeof(DBBase.DefaultInfoMaster)).Cast<Enum>().ToList().ForEach(x =>
                    {
                        wDataTable.Columns.Add(x.ToString());
                    });
                    wDataTable.Rows.Add();
                    #region SetValue
                    wSetValue(DBBase.DefaultInfoMaster.number, "00074");
                    wSetValue(DBBase.DefaultInfoMaster.name, "00000");
                    wSetValue(DBBase.DefaultInfoMaster.working_sartdate, "20180501");
                    wSetValue(DBBase.DefaultInfoMaster.working_enddate, "99999999");
                    wSetValue(DBBase.DefaultInfoMaster.holiday_flag, "0");
                    wSetValue(DBBase.DefaultInfoMaster.working_start, "0700");
                    wSetValue(DBBase.DefaultInfoMaster.working_end, "2000");
                    wSetValue(DBBase.DefaultInfoMaster.break_time,  1.00m);
                    wSetValue(DBBase.DefaultInfoMaster.their_working, 0.00m);
                    wSetValue(DBBase.DefaultInfoMaster.nomal_working, 8.00m);
                    wSetValue(DBBase.DefaultInfoMaster.guests_nomal_working, 8.00m);
                    wSetValue(DBBase.DefaultInfoMaster.guests_over_working, 0.00m);
                    wSetValue(DBBase.DefaultInfoMaster.guests_working, 8.00m);
                    wSetValue(DBBase.DefaultInfoMaster.update_date, "201812241741");
                    wSetValue(DBBase.DefaultInfoMaster.delete_flag, "0");
                    #endregion SeValue
                    break;
            }
            return wDataTable;
        }
    }
}
