﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using static Repotta.DBClass.DepartmentMaster.DepartmentMasterEnum;

namespace Repotta.DBClass
{
    /// <summary>
    /// 勤務マスタ
    /// </summary>
    public class DepartmentMaster : DBTableBase<DepartmentMasterRow>
    {
        #region overrides
        protected override DBTablesEnum DBTable => DBTablesEnum.DepartmentMaster;
        public override List<DepartmentMasterRow> RecordList { get; set; } = new List<DepartmentMasterRow>();
        #endregion overrides

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vDataTable"></param>
        public DepartmentMaster(DataTable vDataTable)
        {
            RecordList = vDataTable.Rows.Cast<DataRow>().Select(x => new DepartmentMasterRow(x)).ToList();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vDicWhere"></param>
        public DepartmentMaster(Dictionary<Enum, WhereParam> vDicWhere)
        {
            RecordList = Execute(() => GetQuerySelect(vDicWhere))?.Rows.Cast<DataRow>()
                         .Select(x => new DepartmentMasterRow(x)).ToList() 
                         ?? new List<DepartmentMasterRow>();
        }
        #endregion コンストラクタ
    }

    /// <summary>
    /// DBレコード
    /// </summary>
    public class DepartmentMasterRow : DBRowBase
    {
        #region overrides
        protected override DBTablesEnum DBTable => DBTablesEnum.DepartmentMaster;
        protected override Type DBFields => typeof(DepartmentMasterEnum);
        protected override string GetValue(Enum vEnum)
        {
            switch (vEnum)
            {
                case code: return Code;
                case name: return Name;
                case update_date: return UpdateDate;
                case delete_flag: return DeleteFlag?.ToString();
                default: return "";
            }
        }
        #endregion overrides

        #region Columns
        /// <summary> 部署コード </summary>
        public string Code { get; set; }
        /// <summary> 部署名 </summary>
        public string Name { get; set; }
        /// <summary> 登録日時 </summary>
        public string UpdateDate { get; set; }
        /// <summary> 削除フラグ </summary>
        public int? DeleteFlag { get; set; }
        #endregion Columns

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vDataRow"></param>
        public DepartmentMasterRow(DataRow vDataRow)
        {
            Code = ToString(vDataRow, code);
            Name = ToString(vDataRow, name);
            UpdateDate = ToString(vDataRow, update_date);
            DeleteFlag = ToIntOrNull(vDataRow, delete_flag);
        }
        #endregion コンストラクタ
    }
}
