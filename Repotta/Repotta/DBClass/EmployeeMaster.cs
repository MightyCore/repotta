﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using static Repotta.DBClass.EmployeeMaster.EmployeeMasterEnum;

namespace Repotta.DBClass
{
    /// <summary>
    /// 勤務マスタ
    /// </summary>
    public class EmployeeMaster : DBTableBase<EmployeeMasterRow>
    {
        #region overrides
        protected override DBTablesEnum DBTable => DBTablesEnum.EmployeeMaster;
        public override List<EmployeeMasterRow> RecordList { get; set; } = new List<EmployeeMasterRow>();
        #endregion overrides

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vDataTable"></param>
        public EmployeeMaster(DataTable vDataTable)
        {
            RecordList = vDataTable.Rows.Cast<DataRow>().Select(x => new EmployeeMasterRow(x)).ToList();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vDicWhere"></param>
        public EmployeeMaster(Dictionary<Enum, WhereParam> vDicWhere)
        {
            RecordList = Execute(() => GetQuerySelect(vDicWhere))?.Rows.Cast<DataRow>()
                         .Select(x => new EmployeeMasterRow(x)).ToList()
                         ?? new List<EmployeeMasterRow>();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vUserID"></param>
        public EmployeeMaster(string vUserID) : this(new Dictionary<Enum, WhereParam> {
            { number, new WhereParam(vUserID) },
            { delete_flag, new WhereParam(0) }})
        { }
        #endregion コンストラクタ
    }

    /// <summary>
    /// DBレコード
    /// </summary>
    public class EmployeeMasterRow : DBRowBase
    {
        #region overrides
        protected override DBTablesEnum DBTable => DBTablesEnum.EmployeeMaster;
        protected override Type DBFields => typeof(EmployeeMasterEnum);
        protected override string GetValue(Enum vEnum)
        {
            switch (vEnum)
            {
                case number: return Number;
                case name: return Name;
                case post_code: return PostCode;
                case company_code: return CompanyCode;
                case department_code: return DepartmentCode;
                case update_date: return UpdateDate;
                case delete_flag: return DeleteFlag?.ToString();
                default: return "";
            }
        }
        #endregion overrides

        #region Columns
        /// <summary> 社員番号 </summary>
        public string Number { get; set; }
        /// <summary> 氏名 </summary>
        public string Name { get; set; }
        /// <summary> 役職コード </summary>
        public string PostCode { get; set; }
        /// <summary> 会社コード </summary>
        public string CompanyCode { get; set; }
        /// <summary> 部署コード </summary>
        public string DepartmentCode { get; set; }
        /// <summary> 登録日時 </summary>
        public string UpdateDate { get; set; }
        /// <summary> 削除フラグ </summary>
        public int? DeleteFlag { get; set; }
        #endregion Columns

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vDataRow"></param>
        public EmployeeMasterRow(DataRow vDataRow)
        {
            Number = ToString(vDataRow, number);
            Name = ToString(vDataRow, name);
            PostCode = ToString(vDataRow, post_code);
            CompanyCode = ToString(vDataRow, company_code);
            DepartmentCode = ToString(vDataRow, department_code);
            UpdateDate = ToString(vDataRow, update_date);
            DeleteFlag = ToIntOrNull(vDataRow, delete_flag);
        }
        #endregion コンストラクタ
    }
}
