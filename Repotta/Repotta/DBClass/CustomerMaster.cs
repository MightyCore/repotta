﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using static Repotta.DBClass.CustomerMaster.CustomerMasterEnum;

namespace Repotta.DBClass
{
    /// <summary>
    /// 勤務マスタ
    /// </summary>
    public class CustomerMaster : DBTableBase<CustomerMasterRow>
    {
        #region overrides
        protected override DBTablesEnum DBTable => DBTablesEnum.CustomerMaster;
        public override List<CustomerMasterRow> RecordList { get; set; } = new List<CustomerMasterRow>();
        #endregion overrides

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vDataTable"></param>
        public CustomerMaster(DataTable vDataTable)
        {
            RecordList = vDataTable.Rows.Cast<DataRow>().Select(x => new CustomerMasterRow(x)).ToList();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vDicWhere"></param>
        public CustomerMaster(Dictionary<Enum, WhereParam> vDicWhere)
        {
            RecordList = Execute(() => GetQuerySelect(vDicWhere))?.Rows.Cast<DataRow>()
                         .Select(x => new CustomerMasterRow(x)).ToList() 
                         ?? new List<CustomerMasterRow>();
        }
        #endregion コンストラクタ
    }

    /// <summary>
    /// DBレコード
    /// </summary>
    public class CustomerMasterRow : DBRowBase
    {
        #region overrides
        protected override DBTablesEnum DBTable => DBTablesEnum.CustomerMaster;
        protected override Type DBFields => typeof(CustomerMasterEnum);
        protected override string GetValue(Enum vEnum)
        {
            switch (vEnum)
            {
                case code: return Code;
                case name: return Name;
                case update_date: return UpdateDate;
                case delete_flag: return DeleteFlag?.ToString();
                default: return "";
            }
        }
        #endregion overrides

        #region Columns
        /// <summary> 客先コード </summary>
        public string Code { get; set; }
        /// <summary> 客先名 </summary>
        public string Name { get; set; }
        /// <summary> 登録日時 </summary>
        public string UpdateDate { get; set; }
        /// <summary> 削除フラグ </summary>
        public int? DeleteFlag { get; set; }
        #endregion Columns

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vDataRow"></param>
        public CustomerMasterRow(DataRow vDataRow)
        {
            Code = ToString(vDataRow, code);
            Name = ToString(vDataRow, name);
            UpdateDate = ToString(vDataRow, update_date);
            DeleteFlag = ToIntOrNull(vDataRow, delete_flag);
        }
        #endregion コンストラクタ
    }
}
