﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using static Repotta.DBClass.DefaultInfoMaster.DefaultInfoMaster;

namespace Repotta.DBClass
{
    /// <summary>
    /// 勤務マスタ
    /// </summary>
    public class DefaultInfoMaster : DBTableBase<DefaultInfoMasterRow>
    {
        #region overrides
        protected override DBTablesEnum DBTable => DBTablesEnum.DefaultInfoMaster;
        public override List<DefaultInfoMasterRow> RecordList { get; set; } = new List<DefaultInfoMasterRow>();
        #endregion overrides

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vDataTable"></param>
        public DefaultInfoMaster(DataTable vDataTable)
        {
            RecordList = vDataTable.Rows.Cast<DataRow>().Select(x => new DefaultInfoMasterRow(x)).ToList();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vDicWhere"></param>
        public DefaultInfoMaster(Dictionary<Enum, WhereParam> vDicWhere)
        {
            RecordList = Execute(() => GetQuerySelect(vDicWhere))?.Rows.Cast<DataRow>().Select(x => new DefaultInfoMasterRow(x)).ToList() ?? new List<DefaultInfoMasterRow>();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vUserID"></param>
        /// <param name="vDate"></param>
        public DefaultInfoMaster(string vUserID, string vDate) : this(new Dictionary<Enum, WhereParam> {
            { number, new WhereParam(vUserID) },
            { working_sartdate, new WhereParam(vDate, QueryParamEnum.NotGreaterThan)},
            { working_enddate, new WhereParam(vDate, QueryParamEnum.NotLessThan)}})
        { }
        #endregion コンストラクタ
    }

    /// <summary>
    /// DBレコード
    /// </summary>
    public class DefaultInfoMasterRow : DBRowBase
    {
        #region overrides
        protected override DBTablesEnum DBTable => DBTablesEnum.DefaultInfoMaster;
        protected override Type DBFields => typeof(DefaultInfoMaster);
        protected override string GetValue(Enum vEnum)
        {
            switch (vEnum)
            {
                case number: return Number;
                case name: return Name;
                case working_sartdate: return WorkingSartdate;
                case working_enddate: return WorkingEnddate;
                case holiday_flag: return HolidayFlag.ToString();
                case working_start: return WorkingStart;
                case working_end: return WorkingEnd;
                case break_time: return BreakTime.ToString();
                case their_working: return TheirWorking.ToString();
                case nomal_working: return NomalWorking.ToString();
                case guests_nomal_working: return GuestsNomalWorking.ToString();
                case guests_over_working: return GuestsOverWorking.ToString();
                case guests_working: return GuestsWorking.ToString();
                case update_date: return UpdateDate;
                case delete_flag: return DeleteFlag?.ToString();
                default: return "";
            }
        }
        #endregion overrides

        #region Columns
        /// <summary> 社員番号 </summary>
        public string Number { get; set; }
        /// <summary> 客先コード </summary>
        public string Name { get; set; }
        /// <summary> 勤務開始日付 </summary>
        public string WorkingSartdate { get; set; }
        /// <summary> 勤務終了日付 </summary>
        public string WorkingEnddate { get; set; }
        /// <summary> 休日フラグ </summary>
        public int HolidayFlag { get; set; }
        /// <summary> 開始時間 </summary>
        public string WorkingStart { get; set; }
        /// <summary> 終了時間 </summary>
        public string WorkingEnd { get; set; }
        /// <summary> 休憩時間 </summary>
        public Decimal BreakTime { get; set; }
        /// <summary> 自社勤務時間 </summary>
        public Decimal TheirWorking { get; set; }
        /// <summary> 通常勤務時間 </summary>
        public Decimal NomalWorking { get; set; }
        /// <summary> 客先通常勤務時間 </summary>
        public Decimal GuestsNomalWorking { get; set; }
        /// <summary> 客先残業勤務時間 </summary>
        public Decimal GuestsOverWorking { get; set; }
        /// <summary> 客先合計勤務時間 </summary>
        public Decimal GuestsWorking { get; set; }
        /// <summary> 登録日時 </summary>
        public string UpdateDate { get; set; }
        /// <summary> 削除フラグ </summary>
        public int? DeleteFlag { get; set; }
        #endregion Columns

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vDataRow"></param>
        public DefaultInfoMasterRow(DataRow vDataRow)
        {
            Number = ToString(vDataRow, number);
            Name = ToString(vDataRow, name);
            WorkingSartdate = ToString(vDataRow, working_sartdate);
            WorkingEnddate = ToString(vDataRow, working_enddate);
            HolidayFlag = ToInt(vDataRow, holiday_flag);
            WorkingStart = ToString(vDataRow, working_start);
            WorkingEnd = ToString(vDataRow, working_end);
            BreakTime = ToDec(vDataRow, break_time);
            TheirWorking = ToDec(vDataRow, their_working);
            NomalWorking = ToDec(vDataRow, nomal_working);
            GuestsNomalWorking = ToDec(vDataRow, guests_nomal_working);
            GuestsOverWorking = ToDec(vDataRow, guests_over_working);
            GuestsWorking = ToDec(vDataRow, guests_working);
            UpdateDate = ToString(vDataRow, update_date);
            DeleteFlag = ToIntOrNull(vDataRow, delete_flag);
        }
        #endregion コンストラクタ
    }
}
