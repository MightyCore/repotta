﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using static Repotta.DBClass.WorkingMaster.WorkingMasterEnum;

namespace Repotta.DBClass
{
    /// <summary>
    /// 勤務マスタ
    /// </summary>
    public class WorkingMaster : DBTableBase<WorkingMasterRow>
    {
        #region overrides
        protected override DBTablesEnum DBTable => DBTablesEnum.WorkingMaster;
        public override List<WorkingMasterRow> RecordList { get; set; } = new List<WorkingMasterRow>();
        #endregion overrides

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vDataTable"></param>
        public WorkingMaster(DataTable vDataTable)
        {
            RecordList = vDataTable.Rows.Cast<DataRow>().Select(x => new WorkingMasterRow(x)).ToList();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vDicWhere"></param>
        public WorkingMaster(Dictionary<Enum, WhereParam> vDicWhere)
        {
            RecordList = Execute(() => GetQuerySelect(vDicWhere))?.Rows.Cast<DataRow>()
                         .Select(x => new WorkingMasterRow(x)).ToList()
                         ?? new List<WorkingMasterRow>();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vUserID"></param>
        /// <param name="vDate"></param>
        public WorkingMaster(string vUserID, string vDate) : this(new Dictionary<Enum, WhereParam> {
            { employee_number, new WhereParam(vUserID) },
            { working_date, new WhereParam(vDate)}})
        { }
        #endregion コンストラクタ
    }

    /// <summary>
    /// DBレコード
    /// </summary>
    public class WorkingMasterRow : DBRowBase
    {
        #region overrides
        protected override DBTablesEnum DBTable => DBTablesEnum.WorkingMaster;
        protected override Type DBFields => typeof(WorkingMasterEnum);
        protected override string GetValue(Enum vEnum)
        {
            switch (vEnum)
            {
                case working_date: return WorkingDate;
                case employee_number: return EmployeeNumber;
                case post_name: return PostName;
                case company_name: return CompanyName;
                case department_name: return DepartmentName;
                case holiday_flag: return HolidayFlag.ToString();
                case working_start: return WorkingStart;
                case working_end: return WorkingEnd;
                case break_time: return BreakTime.ToString();
                case their_working_time: return TheirWorkingTime.ToString();
                case nomal_working: return NomalWorking.ToString();
                case their_working_number: return TheirWorkingNumber.ToString();
                case guests_nomal_working: return GuestsNomalWorking.ToString();
                case guests_over_working: return GuestsOverWorking.ToString();
                case guests_working: return GuestsWorking.ToString();
                case weekdays_over: return WeekdaysOver?.ToString();
                case weekdays_night: return WeekdaysNight?.ToString();
                case holiday_over: return HolidayOver?.ToString();
                case holiday_night: return HolidayNight?.ToString();
                case vacation_flag: return VacationFlag?.ToString();
                case vacation_type: return VacationType?.ToString();
                case absence_flag: return AbsenceFlag?.ToString();
                case absence_session_flag: return AbsenceSessionFlag?.ToString();
                case late_early_out_flag: return LateEarlyOutFlag?.ToString();
                case late_early_out_type: return LateEarlyOutType?.ToString();
                case late_early_out_session_type: return LateEarlyOutSessionType?.ToString();
                case late_early_out_time: return LateEarlyOutTime;
                case remarks: return Remarks;
                case update_date: return UpdateDate;
                default: return "";
            }
        }
        #endregion overrides

        #region Columns
        /// <summary> 年月日 </summary>
        public string WorkingDate { get; set; }
        /// <summary> 社員番号 </summary>
        public string EmployeeNumber { get; set; }
        /// <summary> 役職名 </summary>
        public string PostName { get; set; }
        /// <summary> 会社名 </summary>
        public string CompanyName { get; set; }
        /// <summary> 部署名 </summary>
        public string DepartmentName { get; set; }
        /// <summary> 休日フラグ </summary>
        public int HolidayFlag { get; set; }
        /// <summary> 開始時間 </summary>
        public string WorkingStart { get; set; }
        /// <summary> 終了時間 </summary>
        public string WorkingEnd { get; set; }
        /// <summary> 休憩時間 </summary>
        public Decimal BreakTime { get; set; }
        /// <summary> 自社勤務時間 </summary>
        public Decimal TheirWorkingTime { get; set; }
        /// <summary> 通常勤務時間 </summary>
        public Decimal NomalWorking { get; set; }
        /// <summary> 自社業務工数 </summary>
        public Decimal TheirWorkingNumber { get; set; }
        /// <summary> 客先通常勤務時間 </summary>
        public Decimal GuestsNomalWorking { get; set; }
        /// <summary> 客先残業勤務時間 </summary>
        public Decimal GuestsOverWorking { get; set; }
        /// <summary> 客先合計勤務時間 </summary>
        public Decimal GuestsWorking { get; set; }
        /// <summary> 平日時間外 </summary>
        public Decimal? WeekdaysOver { get; set; }
        /// <summary> 平日深夜 </summary>
        public Decimal? WeekdaysNight { get; set; }
        /// <summary> 休日時間外 </summary>
        public Decimal? HolidayOver { get; set; }
        /// <summary> 休日深夜 </summary>
        public Decimal? HolidayNight { get; set; }
        /// <summary> 休暇フラグ </summary>
        public int? VacationFlag { get; set; }
        /// <summary> 休暇種別 </summary>
        public int? VacationType { get; set; }
        /// <summary> 欠勤フラグ </summary>
        public int? AbsenceFlag { get; set; }
        /// <summary> 欠勤届フラグ </summary>
        public int? AbsenceSessionFlag { get; set; }
        /// <summary> 遅刻・早退・私用外出フラグ </summary>
        public int? LateEarlyOutFlag { get; set; }
        /// <summary> 遅刻・早退・私用外出種別 </summary>
        public int? LateEarlyOutType { get; set; }
        /// <summary> 遅刻・早退・私用外出届フラグ </summary>
        public int? LateEarlyOutSessionType { get; set; }
        /// <summary> 遅刻・早退・私用外出時間 </summary>
        public string LateEarlyOutTime { get; set; }
        /// <summary> 備考 </summary>
        public string Remarks { get; set; }
        /// <summary> 登録日時 </summary>
        public string UpdateDate { get; set; }
        #endregion Columns

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vDataRow"></param>
        public WorkingMasterRow(DataRow vDataRow)
        {
            WorkingDate = ToString(vDataRow, working_date);
            EmployeeNumber = ToString(vDataRow, employee_number);
            PostName = ToString(vDataRow, post_name);
            CompanyName = ToString(vDataRow, company_name);
            DepartmentName = ToString(vDataRow, department_name);
            HolidayFlag = ToInt(vDataRow, holiday_flag);
            WorkingStart = ToString(vDataRow, working_start);
            WorkingEnd = ToString(vDataRow, working_end);
            BreakTime = ToDec(vDataRow, break_time);
            TheirWorkingTime = ToDec(vDataRow, their_working_time);
            NomalWorking = ToDec(vDataRow, nomal_working);
            TheirWorkingNumber = ToDec(vDataRow, their_working_number);
            GuestsNomalWorking = ToDec(vDataRow, guests_nomal_working);
            GuestsOverWorking = ToDec(vDataRow, guests_over_working);
            GuestsWorking = ToDec(vDataRow, guests_working);
            WeekdaysOver = ToDecOrNull(vDataRow, weekdays_over);
            WeekdaysNight = ToDecOrNull(vDataRow, weekdays_night);
            HolidayOver = ToDecOrNull(vDataRow, holiday_over);
            HolidayNight = ToDecOrNull(vDataRow, holiday_night);
            VacationFlag = ToIntOrNull(vDataRow, vacation_flag);
            VacationType = ToIntOrNull(vDataRow, vacation_type);
            AbsenceFlag = ToIntOrNull(vDataRow, absence_flag);
            AbsenceSessionFlag = ToIntOrNull(vDataRow, absence_session_flag);
            LateEarlyOutFlag = ToIntOrNull(vDataRow, late_early_out_flag);
            LateEarlyOutType = ToIntOrNull(vDataRow, late_early_out_type);
            LateEarlyOutSessionType = ToIntOrNull(vDataRow, late_early_out_session_type);
            LateEarlyOutTime = ToString(vDataRow, late_early_out_time);
            Remarks = ToString(vDataRow, remarks);
            UpdateDate = ToString(vDataRow, update_date);
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vEmployeeMasterRow"></param>
        public WorkingMasterRow(EmployeeMasterRow vEmployeeMasterRow)
        {
            EmployeeNumber = vEmployeeMasterRow.Number;
            PostName = vEmployeeMasterRow.PostCode;
            CompanyName = vEmployeeMasterRow.CompanyCode;
            DepartmentName = vEmployeeMasterRow.DepartmentCode;
        }
        #endregion コンストラクタ
    }
}
