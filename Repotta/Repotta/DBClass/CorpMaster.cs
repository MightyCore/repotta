﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using static Repotta.DBClass.CorpMaster.CorpMasterEnum;

namespace Repotta.DBClass
{
    /// <summary>
    /// 勤務マスタ
    /// </summary>
    public class CorpMaster : DBTableBase<CorpMasterRow>
    {
        #region overrides
        protected override DBTablesEnum DBTable => DBTablesEnum.CorpMaster;
        public override List<CorpMasterRow> RecordList { get; set; } = new List<CorpMasterRow>();
        #endregion overrides

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vDataTable"></param>
        public CorpMaster(DataTable vDataTable)
        {
            RecordList = vDataTable.Rows.Cast<DataRow>().Select(x => new CorpMasterRow(x)).ToList();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vDicWhere"></param>
        public CorpMaster(Dictionary<Enum, WhereParam> vDicWhere)
        {
            RecordList = Execute(() => GetQuerySelect(vDicWhere))?.Rows.Cast<DataRow>()
                         .Select(x => new CorpMasterRow(x)).ToList() 
                         ?? new List<CorpMasterRow>();
        }
        #endregion コンストラクタ
    }

    /// <summary>
    /// DBレコード
    /// </summary>
    public class CorpMasterRow : DBRowBase
    {
        #region overrides
        protected override DBTablesEnum DBTable => DBTablesEnum.CorpMaster;
        protected override Type DBFields => typeof(CorpMasterEnum);
        protected override string GetValue(Enum vEnum)
        {
            switch (vEnum)
            {
                case code: return Code;
                case name: return Name;
                case night_start: return NightStart;
                case night_end: return NightEnd;
                case update_date: return UpdateDate;
                case delete_flag: return DeleteFlag?.ToString();
                default: return "";
            }
        }
        #endregion overrides

        #region Columns
        /// <summary> 会社コード </summary>
        public string Code { get; set; }
        /// <summary> 会社名 </summary>
        public string Name { get; set; }
        /// <summary> 深夜開始時間 </summary>
        public string NightStart { get; set; }
        /// <summary> 深夜終了時間 </summary>
        public string NightEnd { get; set; }
        /// <summary> 登録日時 </summary>
        public string UpdateDate { get; set; }
        /// <summary> 削除フラグ </summary>
        public int? DeleteFlag { get; set; }
        #endregion Columns

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vDataRow"></param>
        public CorpMasterRow(DataRow vDataRow)
        {
            Code = ToString(vDataRow, code);
            Name = ToString(vDataRow, name);
            NightStart = ToString(vDataRow, night_start);
            NightEnd = ToString(vDataRow, night_end);
            UpdateDate = ToString(vDataRow, update_date);
            DeleteFlag = ToIntOrNull(vDataRow, delete_flag);
        }
        #endregion コンストラクタ
    }
}
