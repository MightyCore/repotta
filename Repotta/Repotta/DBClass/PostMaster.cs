﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using static Repotta.DBClass.PostMaster.PostMasterEnum;

namespace Repotta.DBClass
{
    /// <summary>
    /// 勤務マスタ
    /// </summary>
    public class PostMaster : DBTableBase<PostMasterRow>
    {
        #region overrides
        protected override DBTablesEnum DBTable => DBTablesEnum.PostMaster;
        public override List<PostMasterRow> RecordList { get; set; } = new List<PostMasterRow>();
        #endregion overrides

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vDataTable"></param>
        public PostMaster(DataTable vDataTable)
        {
            RecordList = vDataTable.Rows.Cast<DataRow>().Select(x => new PostMasterRow(x)).ToList();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vDicWhere"></param>
        public PostMaster(Dictionary<Enum, WhereParam> vDicWhere)
        {
            RecordList = Execute(() => GetQuerySelect(vDicWhere))?.Rows.Cast<DataRow>()
                         .Select(x => new PostMasterRow(x)).ToList() 
                         ?? new List<PostMasterRow>();
        }
        #endregion コンストラクタ
    }

    /// <summary>
    /// DBレコード
    /// </summary>
    public class PostMasterRow : DBRowBase
    {
        #region overrides
        protected override DBTablesEnum DBTable => DBTablesEnum.PostMaster;
        protected override Type DBFields => typeof(PostMasterEnum);
        protected override string GetValue(Enum vEnum)
        {
            switch (vEnum)
            {
                case code: return Code;
                case name: return Name;
                case update_date: return UpdateDate;
                case delete_flag: return DeleteFlag?.ToString();
                default: return "";
            }
        }
        #endregion overrides

        #region Columns
        /// <summary> 役職コード </summary>
        public string Code { get; set; }
        /// <summary> 役職名 </summary>
        public string Name { get; set; }
        /// <summary> 登録日時 </summary>
        public string UpdateDate { get; set; }
        /// <summary> 削除フラグ </summary>
        public int? DeleteFlag { get; set; }
        #endregion Columns

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vDataRow"></param>
        public PostMasterRow(DataRow vDataRow)
        {
            Code = ToString(vDataRow, code);
            Name = ToString(vDataRow, name);
            UpdateDate = ToString(vDataRow, update_date);
            DeleteFlag = ToIntOrNull(vDataRow, delete_flag);
        }
        #endregion コンストラクタ
    }
}
