﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using static Repotta.Common.Attributes;

namespace Repotta.DBClass
{
    public abstract class DBTableBase<T> : DBBase where T : DBRowBase
    {
        #region abstracts
        public abstract List<T> RecordList { get; set; }
        #endregion abstracts

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public DBTableBase() { }
        #endregion コンストラクタ

        #region Query
        /// <summary>
        /// INSERT文作成
        /// </summary>
        /// <param name="vDicWhere"></param>
        /// <returns></returns>
        public string GetQueryInsert(Dictionary<Enum, WhereParam> vDicWhere = null)
        {
            string wQuery = $"INSERT INTO {DBTable} VALUES {Environment.NewLine}";
            wQuery += RecordList.Select(x => x.GetParamInsert()).Aggregate((x, y) => $"{x}, {y}");
            if ((vDicWhere?.Count ?? 0) > 0) wQuery += GetQueryWhere(vDicWhere);
            return wQuery;
        }

        /// <summary>
        /// SELECT文作成
        /// </summary>
        /// <param name="vDicWhere"></param>
        /// <returns></returns>
        public string GetQuerySelect(Dictionary<Enum, WhereParam> vDicWhere = null)
        {
            string wQuery = $"SELECT * FROM {DBTable}{Environment.NewLine}";
            if ((vDicWhere?.Count ?? 0) > 0) wQuery += GetQueryWhere(vDicWhere);
            return wQuery;
        }
        #endregion Query
    }

    public abstract class DBRowBase : DBBase
    {
        #region abstracts
        protected abstract Type DBFields { get; }
        protected abstract string GetValue(Enum vEnum);
        #endregion abstracts

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public DBRowBase() { }
        #endregion コンストラクタ

        #region 値取得
        /// <summary>
        /// DataRowから文字列取得
        /// </summary>
        /// <param name="vDataRow"></param>
        /// <param name="vEnum"></param>
        /// <returns></returns>
        protected string ToString(DataRow vDataRow, Enum vEnum) =>
            ExistCol(vDataRow, vEnum) ? vDataRow[vEnum.ToString()]?.ToString() ?? "" : "";

        /// <summary>
        /// DataRowから数値取得
        /// </summary>
        /// <param name="vDataRow"></param>
        /// <param name="vEnum"></param>
        /// <returns></returns>
        protected int ToInt(DataRow vDataRow, Enum vEnum) => ToIntOrNull(vDataRow, vEnum) ?? 0;

        /// <summary>
        /// DaraRowからDecimal取得
        /// </summary>
        /// <param name="vDataRow"></param>
        /// <param name="vEnum"></param>
        /// <returns></returns>
        protected Decimal ToDec(DataRow vDataRow, Enum vEnum) => ToDecOrNull(vDataRow, vEnum) ?? 0m;

        /// <summary>
        /// DataRowから数値またはNull取得
        /// </summary>
        /// <param name="vDataRow"></param>
        /// <param name="vEnum"></param>
        /// <returns></returns>
        protected int? ToIntOrNull(DataRow vDataRow, Enum vEnum) =>
            ExistCol(vDataRow, vEnum)
            ? int.TryParse(vDataRow[vEnum.ToString()].ToString(), out int wIntValue) ? wIntValue : (int?)null
            : null;

        /// <summary>
        /// DataRowからDecimalまたはNull取得
        /// </summary>
        /// <param name="vDataRow"></param>
        /// <param name="vEnum"></param>
        /// <returns></returns>
        protected Decimal? ToDecOrNull(DataRow vDataRow, Enum vEnum) =>
            ExistCol(vDataRow, vEnum)
            ? Decimal.TryParse(vDataRow[vEnum.ToString()].ToString(), out Decimal wIntValue) ? wIntValue : (Decimal?)null
            : null;

        /// <summary>
        /// 対象列が存在するか確認
        /// </summary>
        /// <param name="vDataRow"></param>
        /// <param name="vEnum"></param>
        /// <returns></returns>
        private bool ExistCol(DataRow vDataRow, Enum vEnum) =>
            vDataRow.Table.Columns.Contains(vEnum.ToString());
        #endregion 値取得

        #region DB更新
        /// <summary>
        /// UPDATE文作成
        /// </summary>
        /// <param name="vDicWhere"></param>
        /// <returns></returns>
        public string GetQueryUpdate(Dictionary<Enum, WhereParam> vDicWhere = null)
        {
            string wQuery = $"UPDATE {DBTable}{Environment.NewLine} SET {Environment.NewLine}";
            wQuery += Enum.GetValues(DBFields).Cast<Enum>()
                .Select(x => GetParamSetter(new KeyValuePair<Enum, WhereParam>(x, new WhereParam(GetValue(x)))))
                .Aggregate((x, y) => $"{x}, {y}");
            if ((vDicWhere?.Count ?? 0) > 0) wQuery += GetQueryWhere(vDicWhere);
            return wQuery;
        }

        /// <summary>
        /// 挿入用の1レコード分のInsert文作成
        /// </summary>
        /// <returns></returns>
        public string GetParamInsert() =>
            $"({Enum.GetValues(DBFields).Cast<Enum>().Select(x => $"'{GetValue(x)}'").Aggregate((x, y) => $"{x}, {y}")}){Environment.NewLine}";
        #endregion DB更新
    }

    public abstract class DBBase
    {
        #region abstracts
        protected abstract DBTablesEnum DBTable { get; }
        #endregion abstracts

        #region QueryParam
        /// <summary>
        /// Where区作成補助クラス
        /// </summary>
        public class WhereParam
        {
            /// <summary> Where区補助記号 </summary>
            public QueryParamEnum QueryParamEnum { get; set; }
            /// <summary> 値 </summary>
            public object Value { get; set; }

            /// <summary>
            /// コンストラクタ
            /// </summary>
            /// <param name="vValue"></param>
            /// <param name="vQueryParamEnum"></param>
            public WhereParam(object vValue, QueryParamEnum vQueryParamEnum = QueryParamEnum.Equal)
            {
                QueryParamEnum = vQueryParamEnum;
                Value = vValue;
            }
        }

        /// <summary>
        /// Where区補助記号
        /// </summary>
        public enum QueryParamEnum : int
        {
            /// <summary> = </summary>
            [Text(Text = "=")]
            Equal = 0,
            /// <summary> ＞ </summary>
            [Text(Text = ">")]
            GreaterThan,
            /// <summary> ＜ </summary>
            [Text(Text = "<")]
            LessThan,
            /// <summary> ≦ </summary>
            [Text(Text = "<=")]
            NotGreaterThan,
            /// <summary> ≧ </summary>
            [Text(Text = ">=")]
            NotLessThan
        }
        #endregion QueryParam

        #region Enum
        /// <summary>
        /// テーブル名称
        /// </summary>
        public enum DBTablesEnum : int
        {
            /// <summary> 社員マスタ </summary>
            [Text(Text = "社員マスタ")]
            EmployeeMaster = 1,
            /// <summary> 役職マスタ </summary>
            [Text(Text = "役職マスタ")]
            PostMaster,
            /// <summary> 会社マスタ </summary>
            [Text(Text = "会社マスタ")]
            CorpMaster,
            /// <summary> 部署マスタ </summary>
            [Text(Text = "部署マスタ")]
            DepartmentMaster,
            /// <summary> 客先マスタ </summary>
            [Text(Text = "客先マスタ")]
            CustomerMaster,
            /// <summary> 基本勤怠情報マスタ </summary>
            [Text(Text = "基本勤怠情報マスタ")]
            DefaultInfoMaster,
            /// <summary> 勤務マスタ </summary>
            [Text(Text = "勤務マスタ")]
            WorkingMaster
        }

        /// <summary>
        /// 社員マスタ
        /// </summary>
        public enum EmployeeMasterEnum : int
        {
            /// <summary> 社員番号 </summary>
            [Text(Text = "社員番号")]
            number = 1,
            /// <summary> 氏名 </summary>
            [Text(Text = "氏名")]
            name,
            /// <summary> 役職コード </summary>
            [Text(Text = "役職コード")]
            post_code,
            /// <summary> 会社コード </summary>
            [Text(Text = "会社コード")]
            company_code,
            /// <summary> 部署コード </summary>
            [Text(Text = "部署コード")]
            department_code,
            /// <summary> 登録日時 </summary>
            [Text(Text = "登録日時")]
            update_date,
            /// <summary> 削除フラグ </summary>
            [Text(Text = "削除フラグ")]
            delete_flag
        }

        /// <summary>
        /// 役職マスタ
        /// </summary>
        public enum PostMasterEnum : int
        {
            /// <summary> 役職コード </summary>
            [Text(Text = "役職コード")]
            code = 1,
            /// <summary> 役職名 </summary>
            [Text(Text = "役職名")]
            name,
            /// <summary> 登録日時 </summary>
            [Text(Text = "登録日時")]
            update_date,
            /// <summary> 削除フラグ </summary>
            [Text(Text = "削除フラグ")]
            delete_flag
        }

        /// <summary>
        /// 会社マスタ
        /// </summary>
        public enum CorpMasterEnum : int
        {
            /// <summary> 会社コード </summary>
            [Text(Text = "会社コード")]
            code = 1,
            /// <summary> 会社名 </summary>
            [Text(Text = "会社名")]
            name,
            /// <summary> 深夜開始時間 </summary>
            [Text(Text = "深夜開始時間")]
            night_start,
            /// <summary> 深夜終了時間 </summary>
            [Text(Text = "深夜終了時間")]
            night_end,
            /// <summary> 登録日時 </summary>
            [Text(Text = "登録日時")]
            update_date,
            /// <summary> 削除フラグ </summary>
            [Text(Text = "削除フラグ")]
            delete_flag
        }

        /// <summary>
        /// 部署マスタ
        /// </summary>
        public enum DepartmentMasterEnum : int
        {
            /// <summary> 部署コード </summary>
            [Text(Text = "部署コード")]
            code = 1,
            /// <summary> 部署名 </summary>
            [Text(Text = "部署名")]
            name,
            /// <summary> 登録日時 </summary>
            [Text(Text = "登録日時")]
            update_date,
            /// <summary> 削除フラグ </summary>
            [Text(Text = "削除フラグ")]
            delete_flag
        }

        /// <summary>
        /// 客先マスタ
        /// </summary>
        public enum CustomerMasterEnum : int
        {
            /// <summary> 客先コード </summary>
            [Text(Text = "客先コード")]
            code = 1,
            /// <summary> 客先名 </summary>
            [Text(Text = "客先名")]
            name,
            /// <summary> 登録日時 </summary>
            [Text(Text = "登録日時")]
            update_date,
            /// <summary> 削除フラグ </summary>
            [Text(Text = "削除フラグ")]
            delete_flag
        }

        /// <summary>
        /// 勤務マスタ
        /// </summary>
        public enum WorkingMasterEnum : int
        {
            /// <summary> 年月日 </summary>
            [Text(Text = "年月日")]
            working_date = 1,
            /// <summary> 社員番号 </summary>
            [Text(Text = "社員番号")]
            employee_number,
            /// <summary> 役職名 </summary>
            [Text(Text = "役職名")]
            post_name,
            /// <summary> 会社名 </summary>
            [Text(Text = "会社名")]
            company_name,
            /// <summary> 部署名 </summary>
            [Text(Text = "部署名")]
            department_name,
            /// <summary> 休日フラグ </summary>
            [Text(Text = "休日フラグ")]
            holiday_flag,
            /// <summary> 開始時間 </summary>
            [Text(Text = "開始時間")]
            working_start,
            /// <summary> 終了時間 </summary>
            [Text(Text = "終了時間")]
            working_end,
            /// <summary> 休憩時間 </summary>
            [Text(Text = "休憩時間")]
            break_time,
            /// <summary> 自社勤務時間 </summary>
            [Text(Text = "自社勤務時間")]
            their_working_time,
            /// <summary> 通常勤務時間 </summary>
            [Text(Text = "通常勤務時間")]
            nomal_working,
            /// <summary> 自社業務工数 </summary>
            [Text(Text = "自社業務工数")]
            their_working_number,
            /// <summary> 客先通常勤務時間 </summary>
            [Text(Text = "客先通常勤務時間")]
            guests_nomal_working,
            /// <summary> 客先残業勤務時間 </summary>
            [Text(Text = "客先残業勤務時間")]
            guests_over_working,
            /// <summary> 客先合計勤務時間 </summary>
            [Text(Text = "客先合計勤務時間")]
            guests_working,
            /// <summary> 平日時間外 </summary>
            [Text(Text = "平日時間外")]
            weekdays_over,
            /// <summary> 平日深夜 </summary>
            [Text(Text = "平日深夜")]
            weekdays_night,
            /// <summary> 休日時間外 </summary>
            [Text(Text = "休日時間外")]
            holiday_over,
            /// <summary> 休日深夜 </summary>
            [Text(Text = "休日深夜")]
            holiday_night,
            /// <summary> 休暇フラグ </summary>
            [Text(Text = "休暇フラグ")]
            vacation_flag,
            /// <summary> 休暇種別 </summary>
            [Text(Text = "休暇種別")]
            vacation_type,
            /// <summary> 欠勤フラグ </summary>
            [Text(Text = "欠勤フラグ")]
            absence_flag,
            /// <summary> 欠勤届フラグ </summary>
            [Text(Text = "欠勤届フラグ")]
            absence_session_flag,
            /// <summary> 遅刻・早退・私用外出フラグ </summary>
            [Text(Text = "遅刻・早退・私用外出フラグ")]
            late_early_out_flag,
            /// <summary> 遅刻・早退・私用外出種別 </summary>
            [Text(Text = "遅刻・早退・私用外出種別")]
            late_early_out_type,
            /// <summary> 遅刻・早退・私用外出届フラグ </summary>
            [Text(Text = "遅刻・早退・私用外出届フラグ")]
            late_early_out_session_type,
            /// <summary> 遅刻・早退・私用外出時間 </summary>
            [Text(Text = "遅刻・早退・私用外出時間")]
            late_early_out_time,
            /// <summary> 備考 </summary>
            [Text(Text = "備考")]
            remarks,
            /// <summary> 登録日時 </summary>
            [Text(Text = "登録日時")]
            update_date
        }

        /// <summary>
        /// 基本勤怠情報マスタ
        /// </summary>
        public enum DefaultInfoMaster : int
        {
            /// <summary> 社員番号 </summary>
            [Text(Text = "社員番号")]
            number = 1,
            /// <summary> 客先コード </summary>
            [Text(Text = "客先コード")]
            name,
            /// <summary> 勤務開始日付 </summary>
            [Text(Text = "勤務開始日付")]
            working_sartdate,
            /// <summary> 勤務終了日付 </summary>
            [Text(Text = "勤務終了日付")]
            working_enddate,
            /// <summary> 休日フラグ </summary>
            [Text(Text = "休日フラグ")]
            holiday_flag,
            /// <summary> 開始時間 </summary>
            [Text(Text = "開始時間")]
            working_start,
            /// <summary> 終了時間 </summary>
            [Text(Text = "終了時間")]
            working_end,
            /// <summary> 休憩時間 </summary>
            [Text(Text = "休憩時間")]
            break_time,
            /// <summary> 自社勤務時間 </summary>
            [Text(Text = "自社勤務時間")]
            their_working,
            /// <summary> 通常勤務時間 </summary>
            [Text(Text = "通常勤務時間")]
            nomal_working,
            /// <summary> 客先通常勤務時間 </summary>
            [Text(Text = "客先通常勤務時間")]
            guests_nomal_working,
            /// <summary> 客先残業勤務時間 </summary>
            [Text(Text = "客先残業勤務時間")]
            guests_over_working,
            /// <summary> 客先合計勤務時間 </summary>
            [Text(Text = "客先合計勤務時間")]
            guests_working,
            /// <summary> 登録日時 </summary>
            [Text(Text = "登録日時")]
            update_date,
            /// <summary> 削除フラグ </summary>
            [Text(Text = "削除フラグ")]
            delete_flag
        }
        #endregion Enum

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public DBBase() { }
        #endregion コンストラクタ

        /// <summary>
        /// Query実行
        /// </summary>
        /// <param name="vGetQuery"></param>
        /// <returns></returns>
        public DataTable Execute(Func<string> vGetQuery)
        {
            var wQuery = vGetQuery();
            //TODO: Query実行            
            return Repotta.Common.Util.GetDummyTable(DBTable);
        }

        #region Query
        /// <summary>
        /// WHERE区作成
        /// </summary>
        /// <param name="vDic"></param>
        /// <returns></returns>
        protected string GetQueryWhere(Dictionary<Enum, WhereParam> vDic)
        {
            if (vDic.Count == 0) return "";
            string wQuery = $"WHERE {Environment.NewLine}";
            wQuery += vDic.Select(x => GetParamSetter(x))
                      .Aggregate((x, y) => $"{x} AND {y}");
            return wQuery;
        }

        /// <summary>
        /// 値セット用数式作成
        /// </summary>
        /// <param name="vKeyVal"></param>
        /// <returns></returns>
        protected string GetParamSetter(KeyValuePair<Enum, WhereParam> vKeyVal) =>
            $"{Enum.GetName(vKeyVal.Key.GetType(), vKeyVal.Key)} {vKeyVal.Value.QueryParamEnum.GetText()} '{vKeyVal.Value.Value?.ToString()}' {Environment.NewLine}";
        #endregion Query
    }
}
